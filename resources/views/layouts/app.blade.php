<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                    <div class="col-md-2">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src={{url('micro2.jpg')}}></img>
                        </a>
                    </div>

                    <div class="col-md-8" id="titre">
                        <div class="row justify-content-center">
                            
                            <h1><a class="navbar-brand" href="{{ url('/') }}">Société des beaux parleurs</a></h1>
                            
                        </div>  
                    </div>

                    <div class="col-md-2" id="login">
                        <ul class="navbar-nav ml-auto float-right">
                        <!-- Authentication Links -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        </ul> 
                    </div>
                
            </div>
        </nav>

        <main class="py-4 container-fluid" id="loginBackground">
            @yield('content')
        </main>
    </div>
</body>
</html>
