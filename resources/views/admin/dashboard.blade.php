@extends('layouts.index')

@section('content') 

<div class="container-fluid" id="adminBackground">
    <div class="container">
        <div class="row">

            <div class="col-md-6" id="membersDisplay">
                <h2>Liste des membres :</h2>
                <table class="table ">
                    <thead class="table-light">
                    <th>#</th>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Rôle</th>
                    </thead>
                    @foreach($members as $member)
                    <tr>
                    <th scope="row">{{ $member->user_id }}</th>
                    <td>{{ $member->user_name }}</td>
                    <td>{{ $member->email }}</td>
                    <td>{{ $member->role }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>

            <div class="col-md-1">
            </div>

            <div class="col-md-5">
                <div class="row justify-content-center">
                    <button class="btn btn-secondary btn-lg redirectButtons"><a href="/home">Aller vers l'accueil</a></button>
                </div>

                <div class="row justify-content-center">
                    <button class="btn btn-secondary btn-lg redirectButtons"><a href="/random">Aller vers la page de tirage au sort</a></button>
                </div>
            
                <div class="row justify-content-center" id="accountCreation">
                    <form method="POST" action="/admin_dashboard">
                        <h2>Créer un compte de membre:</h2>
                      
                            <label class="form-label">Nom du membre:</label>
                            <input class="form-control" name="user_name"></input>
                       
                            <label class="form-label">Email:</label>
                            <input class="form-control" name="email"></input>
                       
                            <label class="form-label">Mot de passe:</label>
                            <input class="form-control" name="password"></input>
                      
                            <label class="form-check-label">Role admin:</label>
                            <input class="form-check-inout" name="role" type="checkbox" value="admin">Oui</input>
                       
                        <button class="btn btn-secondary btn-lg float-right" type="submit">Ajouter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
