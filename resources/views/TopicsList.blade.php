@extends('layouts.index')

@section('content')

<div class="container-fluid" id="homeBackground">
    <div class="container-fluid" id="topicSuggestion">
        <div class="row justify-content-center">
            <form method="POST" action="/home" class=>
                <label class="form-label">Suggérez-nous une idée de sujet d'improvisation:</label>
                <input class="form-control" type="text" name="topic_content" minlength="7" maxlength="255" >
                <button class="btn btn-secondary btn-lg" type="submit">Envoyer</button>
            </form>
        </div>
    
        <div class="row justify-content-center">
            <button class="btn btn-secondary btn-lg" id="buttonRandom"><a href='/random'>Tirage au sort d'un sujet</a></button>
        </div>
    </div>

    <div class="container displayTopics">
        <div class="row justify-content-center">
            <h3>Liste des sujets déjà proposés :</h3>
        </div>

    @foreach($topics as $topic)
        <div class="row justify-content-center">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">{{ $topic->topic_content }}</li>
            </ul>
        </div>
    @endforeach
    

    </div>
</div>


@endsection
