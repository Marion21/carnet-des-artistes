@extends('layouts.index')

@section('content')

<div class="randomBackground">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <button class="btn btn-light" id="randomAccueil"><a href="/home">Retour à l'accueil</a></button>
        </div>

        <div class="row justify-content-center" id="topic">
            <p>{{$randomContent}}</p> 

            @isset($msgTreated)
                {{$msgTreated}}
            
            @endisset
        </div>

    </div>
</div>




@endsection