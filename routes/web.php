<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware;

Route::get('/', function () {
    return redirect ('/login');
}); 

Auth::routes();

//Route::get('/', 'HomeController@index');

Route::get('/home', 'MainController@displayTopics');

Route::post('/home', 'MainController@addTopicToDatabase');

Route::get('/random', 'MainController@randomPicking');

Route::get('/admin_dashboard', 'Admin\DashboardController@displayMembers')/*->middleware('role:admin')*/; 

Route::post('/admin_dashboard','Admin\DashboardController@addMemberAccount');

Route::get('/user_dashboard', 'User\DashboardController@index');