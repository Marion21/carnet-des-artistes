<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->truncate(); 
        DB::table('users')->insert([
          'user_name' => 'the admin user',
          'email' => 'iamadmin@gmail.com',
          'role' => 'admin',        
          'password' => Hash::make('password'),
        ]);
        DB::table('users')->insert([
          'user_name' => 'the basic user',
          'email' => 'iamauser@gmail.com',
          'role' => 'user',
          'password' => Hash::make('password'),
        ]);
      }
}
