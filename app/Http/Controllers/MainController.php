<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Topics;

class MainController extends Controller
{
    function displayTopics(Request $request){

        $topics = DB::table('topics')->select('topic_content')->get();
        
        return view('TopicsList',['topics'=>$topics]);
    }

    function addTopicToDatabase(Request $request){
        $topic_content = $request->input('topic_content');

        DB::table('topics')->insert([
            ['topic_content' => $topic_content,
            'displayed'=>'0'],
        ]);

        return redirect ('/home');
    }

    function randomPicking(){

        $randomTopics=Topics::where('displayed','=',0)->inRandomOrder()->first();

        $randomContent = $randomTopics['topic_content'];

        if(!is_null($randomContent)){

            $randomTopics->displayed =1;
    
            $randomTopics->save();

            return view('RandPicking',['randomContent'=>$randomContent]);

        }else{

            $msgTreated = "Il n'y a plus de sujets à afficher!";
        
            return view('RandPicking',['msgTreated'=>$msgTreated, 'randomContent'=>$randomContent]);

        }
}

}