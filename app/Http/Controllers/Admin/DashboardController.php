<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {
  /* public function __construct() {
    $this->middleware('auth');
  } */
  function addMemberAccount(Request $request){

    $user_name = $request->input('user_name');
    $email = $request->input('email');
    $password = $request->input('password');

    if(isset($_POST['role'])){

        DB::table('users')->insert([
        'user_name' => $user_name,
        'email' => $email,
        'password' => Hash::make($password),
        'role' => 'admin',
        ]);

        return redirect('admin_dashboard');

    }else{

      DB::table('users')->insert([
        'user_name' => $user_name,
        'email' => $email,
        'password' => Hash::make($password),
        'role' => 'user',
      ]);

      return redirect('admin_dashboard');

    }
  }

  function displayMembers(Request $resquest){
    $members = DB::table('users')->get();

    return view('admin.dashboard',['members'=>$members]);

  }


  //@todo Ajouter nouveaux spectacles
  function addNewShows(){


  }
}