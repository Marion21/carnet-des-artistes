<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topics extends Model
{
    //

    protected $table = 'topics';
    
    protected $fillable = [
        'topic_content', 'topic_id', 'displayed'];
}
